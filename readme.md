# Media Server

- run `./setup.sh`
- run `docker-compose up`

- Access `:8989` for sonarr 
- Access `:7878` for radarr 
- Access `:8686` for lidarr 
- Access `:6767` for bazarr
- Access `:8080` for qbitorrent
- Access `:8081` for streama 
