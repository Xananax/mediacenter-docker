mkdir config
mkdir config/sonarr
mkdir config/qbittorrent
mkdir config/radarr
mkdir config/lidarr
mkdir config/bazarr
mkdir data
mkdir data/torrents
mkdir data/torrents/movies
mkdir data/torrents/tv
mkdir data/torrents/music
mkdir data/media/movies
mkdir data/media/tv
mkdir data/media/music
mkdir db
docker-compose pull

